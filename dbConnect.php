<?php

class DB
{
	public $_strHost 		= 'localhost';
	public $_strUser 		= 'root';
	public $_strPassword 	= '';
	public $_strDBName		= 'my_task';
	public $_strTableName	= 'users';
	public $objDB 			= '';


	function __construct()
	{
		$this->objDB = new mysqli($this->_strHost, $this->_strUser, $this->_strPassword, $this->_strDBName);

		if ($this->objDB->connect_errno) {
			echo "Failed to connect to DB" . $objDB->connect_error;
			exit();
		}

		$this->checkTable($this->_strTableName);
	}

	public function checkTable($_tableName = 'users')
	{
		$strQury = "show tables like '$_tableName'";

		$objData = $this->objDB->query($strQury);

		if (!$objData->num_rows) {
			echo "$this->_strTableName not created.";

			$strTableQuery = "create table $_tableName (id int(11) PRIMARY KEY AUTO_INCREMENT, name varchar(255))";

			$this->objDB->query($strTableQuery);

			echo "$_tableName table created successfully";
		}
	}

	function getData($strQuery)
	{
		$objResult = $this->objDB->query($strQuery);
		$objData = [];

		while ($obj =  $objResult->fetch_assoc()) {
			$objData[] = $obj;
		}

		return $objData;
	}

	function insert($arrData)
	{

		$strQuery = 'INSERT INTO ' . $this->_strTableName . ' (' . implode(', ', array_keys($arrData)) . ') VALUES (' . implode(', ', array_map(function ($item) {
			return is_string($item) ? "\"" . $item . "\"" : $item;
		}, array_values($arrData))) . ')';

		$this->execute($strQuery);

		return $this->objDB->insert_id;
	}

	public function prepareData($item)
	{
		return $item; //is_string($item) ? "\"".$item."\"" : $item;
	}

	function update($objData, $arrFilter)
	{
		foreach ($objData as $key => $val) {
			$replace = is_string($val) ? "'" . $val . "'" : $val;

			$strValues[] = $key . ' = ' . $replace;
		}

		$strFilter = '';

		if (!empty($arrFilter)) {
			$strFilter = ' WHERE ';
			foreach ($arrFilter as $key => $value) {
				$finder = is_numeric($value) ? $value : "'" . $value . "'";
				$strFilter .= " $key = " . $finder;
			}
		}

		$strQuery = 'UPDATE ' . $this->_strTableName . ' SET ' . implode(', ', $strValues) . $strFilter;

		$this->execute($strQuery);
	}

	function delete($nID)
	{
		$strQuery = 'DELETE FROM ' . $this->_strTableName  . ' WHERE ID = ' . $nID;
		$this->execute($strQuery);
	}

	private function execute($strQuery)
	{
		//echo $strQuery; die;
		return $this->objDB->query($strQuery);
	}
}

// function getDB()
// {
// 	$objDB = new DB();
// 	//$objDB->getData('select * from users');
// 	echo $objDB->insert(['id' => 3, 'name' => 'name3']);
// }

//getDB();
