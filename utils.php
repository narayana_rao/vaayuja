<?php 
	require_once('dbConnect.php'); 

	function get($arrData)
	{
		$filter = '';

		if($arrData)
			$filter = ' WHERE id = ' . $arrData['id'];

		$strQuery = 'select * from users' . $filter;
		$db = new DB();
		echo json_encode($db->getData($strQuery));
	}

	function add($arrData)
	{
		$db = new DB();
		echo json_encode($db->insert($arrData));
	}

	function delete($arrData)
	{
		$db = new DB();
		echo json_encode($db->delete($arrData['id']));
	}

	function update($arrData, $arrFilter)
	{
		$db = new DB();
		echo json_encode($db->update($arrData, $arrFilter));
	}

	function bootstrap($arrData) {

		$bExecute = true;

		if($arrData['REQUEST_METHOD'] == 'POST') {
			if(!empty($_GET)) {
				$bExecute = false;
				return update($_POST, $_GET);
			}
			$bExecute = false;
			return add($_POST);

		} else if($arrData['REQUEST_METHOD'] == 'GET'){
			if(!empty($_GET)) {
				$bExecute = false;
				return get($_GET);
			}
		} else if($arrData['REQUEST_METHOD'] == 'DELETE'){
			if(!empty($_GET)) {
				$bExecute = false;
				return delete($_GET);
			}
		}
	}

	bootstrap($_SERVER);
?>