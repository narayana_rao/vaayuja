$(document).ready(function() {
  $("#data-table").DataTable();

  $("form").submit(function(e) {
    e.preventDefault();

    url = $("form").attr("action");
    $.ajax({
      type: "POST",
      url: url,
      data: $("form").serialize(),
      success: function(response) {
        console.log(response);
        $("#myModal").modal("hide");
        location.reload();
      },
      error: function(response) {
        console.log(response);
      }
    });
  });

  $(".delete").click(function(e) {
    e.preventDefault();
    let url = $(this).attr("href");

    $.ajax({
      type: "DELETE",
      url: url,
      data: "",
      success: function(response) {
        console.log(response);
        location.reload();
      },
      error: function(response) {
        console.log(response);
      }
    });
  });
});

function loadData(userID) {
  $.ajax({
    type: "GET",
    url: "./utils.php/?id=" + userID,
    data: "",
    datatype: "json",
    success: function(response) {
      console.log(typeof response);
      $("#myModal").modal("show");
      $("form").attr("action", "./utils.php?id=" + userID);
      $('input[name="name"]').val(JSON.parse(response)[0]["name"]);
    },
    error: function(response) {
      console.log(response);
    }
  });
}

function loadAjax(obj) {
  $.ajax({
    type: "",
    url: "",
    data: "",
    success: function(response) {
      console.log(response);
    },
    error: function(response) {
      console.log(response);
    }
  });
}
