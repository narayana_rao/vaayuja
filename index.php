<?php require_once('dbConnect.php'); ?>

<html>

<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<?php
	$objDB = new DB();
	$objDB->checkTable('users');
	$arrData = $objDB->getData("select * from users");
	?>
	<div class="container">
		<div class="row">
			<div><button class="btn btn-success" data-toggle="modal" data-target="#myModal">Add</button></div>
		</div>
		<div class="row">
			<table class="table" id="data-table">
				<thead>
					<th>#</th>
					<th>User</th>
					<th>Action</th>
				</thead>
				<tbody>
					<?php
					foreach ($arrData as $user) {

						echo "<tr>";
						echo "<td>" . $user['id'] . "</td>";
						echo "<td>" . $user['name'] . "</td>";
						echo "<td>";
						echo '<button onclick="loadData(' . $user['id'] . ')"><i class="fa fa-pencil"></i></button>';
						echo "<button><a class='delete' href=./utils.php?id=" . $user['id'] . "><i class='fa fa-trash'></i></a></button>";
						echo "</td>";
						echo "</tr>";
					}
					?>
				</tbody>
			</table>
		</div>
	</div>


	<!-- The Modal -->
	<div class="modal" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title">Add User</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<form action='./utils.php'>
						<input type="text" name="name">

						<input type="submit">
					</form>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>
</body>
<footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="script.js"></script>
</footer>

</html>